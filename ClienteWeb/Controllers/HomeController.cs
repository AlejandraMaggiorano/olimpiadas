﻿
using ClienteWeb.Business;
using ClienteWeb.Models.ViewModels;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UtilitiesCliente.Models.WS;
using UtilitiesCliente.Tools;


/*Video 7: las urls en MVC Api se hacen así:
 * localhost:puerto/controlador sin "controller"/ lo que dice después de public Action Result (acá es Register)
*/
namespace UtilitiesCliente.Controllers
{
    public class HomeController : Controller
    {
       
        [HttpGet]
        public ActionResult Register()
        {
            ClienteWeb.Models.ViewModels.RegisterViewModel modelo = new ClienteWeb.Models.ViewModels.RegisterViewModel();
            return View(modelo);
        }

        [HttpGet]
        public ActionResult LogIn()
        {//Esto hace que redireccione bien a login
            UserAccessViewModel model = new UserAccessViewModel();
            return View(model);
        }

        [HttpGet]
        public ActionResult Cacofonix()
        {
            UserAccessViewModel model = new UserAccessViewModel();
            return View(model);
        }

        [HttpGet]
        public ActionResult CacofonixMedallero()
        {
            return RedirectToAction("Index", "CacofonixMedallero");
            //UserAccessViewModel model = new UserAccessViewModel();
            //return View(model);
        }

        [HttpGet]
        public ActionResult CacofonixScore()
        {
            UserAccessViewModel model = new UserAccessViewModel();
            return View(model);
        }

        [HttpPost]
        public ActionResult Login(UserAccessViewModel model  )
        {
            AccessRequest pedido = new AccessRequest();
            pedido.Username = model.Username;
            pedido.Password = Encrypt.GetSHA256(model.Password);

            RequestUtil ReqUtil = new RequestUtil();
            Reply rta = ReqUtil.Execute<AccessRequest>(Constants.Url.ACCESS, "post", pedido);


            UserResponse rtaUsuario = JsonConvert.DeserializeObject<UserResponse>(JsonConvert.SerializeObject(rta.data));

            if (rta.result == 1)
            {
                Session["User"] = rtaUsuario;
                return RedirectToAction("Index","Score");
            }

            ViewBag.error = "Datos incorrectos";
            return View();
        }

        [HttpGet]
        public ActionResult verMedallero()
        {
            return RedirectToAction("Index", "getMedallero");
            //UserAccessViewModel model = new UserAccessViewModel();
            //return View(model);
        }

        [HttpPost]
        public ActionResult Register(ClienteWeb.Models.ViewModels.RegisterViewModel modelo)
        {
            if (!ModelState.IsValid)
            {
                return View(modelo);
            }

            try { 
            Olimpiadas.Models.Request.User usuario = new Olimpiadas.Models.Request.User();
                usuario.username = modelo.Username;
                usuario.password = modelo.Password;

            UtilitiesCliente.Tools.RequestUtil ReqUtil = new RequestUtil();
            Reply rta = ReqUtil.Execute<Olimpiadas.Models.Request.User>(Constants.Url.REGISTER, "post", usuario);

            } catch (Exception e)
            {

            }
            return View();
        }
    }
}