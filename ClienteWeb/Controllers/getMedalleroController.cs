﻿using ClienteWeb.Business;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UtilitiesCliente.Models.WS;
using UtilitiesCliente.Tools;

namespace ClienteWeb.Controllers
{
    public class getMedalleroController : Controller
    {
        // Visualización de Medalleros 

        [HttpGet]
        public ActionResult Index()
        {
            List<ListEquiposResponse> lst = new List<ListEquiposResponse>();


            RequestUtil ReqUtil = new RequestUtil();
            Reply rta = ReqUtil.Execute<ListEquiposResponse>(Constants.Url.EQUIPOS, "post");

            lst = JsonConvert.DeserializeObject<List<ListEquiposResponse>>(JsonConvert.SerializeObject(rta.data));


            return View(lst);
        }//Index


        [HttpGet]
        public ActionResult getMedallero(int id)
        {
            MedalleroRequest model = new MedalleroRequest();
            model.update = false;
            model.id = id;

            List<ListMedalleroResponse> lst = new List<ListMedalleroResponse>();


            RequestUtil ReqUtil = new RequestUtil();
            Reply rta = ReqUtil.Execute<MedalleroRequest>(Constants.Url.MEDALLEROS, "post", model);

            lst = JsonConvert.DeserializeObject<List<ListMedalleroResponse>>(JsonConvert.SerializeObject(rta.data));

            lst = lst.OrderBy(d => d.id).ToList();

            if (rta.result != 0)
            {
                ListMedalleroResponse recibido = lst.First<ListMedalleroResponse>();
            }

            return View(lst);
        }//Index


    }
}