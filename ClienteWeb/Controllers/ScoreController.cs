﻿using ClienteWeb.Business;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UtilitiesCliente.Models.WS;
using UtilitiesCliente.Tools;

namespace ClienteWeb.Controllers
{
    //ESTE GENERA LA VISTA
    public class ScoreController : BaseController
    {
        // GET: Score
        public ActionResult Index()
        {
            GetSession(); //Importante llamar esto para que se cargue la sesión

            List<ListEventoResponse> lst = new List<ListEventoResponse>();

            SecurityRequest pedido = new SecurityRequest();
            pedido.AccessToken = sesion.AccessToken; //Este sesión es heredado de base controller


            RequestUtil ReqUtil = new RequestUtil();
            Reply rta = ReqUtil.Execute<SecurityRequest>(Constants.Url.SCORES, "post", pedido);


            lst  = JsonConvert.DeserializeObject<List<ListEventoResponse>>(JsonConvert.SerializeObject(rta.data));


            return View(lst);
        }
    }
}