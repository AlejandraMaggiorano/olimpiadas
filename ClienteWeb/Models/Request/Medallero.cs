﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ClienteWeb.Models.Request
{
    public class Medallero
    {
        public int id_medallero { get; set; }
        public int oro { get; set; }
        public int plata { get; set; }
        public int bronce { get; set; }
    }
}