﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNet.SignalR;

namespace Olimpiadas
{
    public class counterHub : Hub
    {
       
           public override Task OnConnected()
        {
            Clients.All.enterUser();
            return base.OnConnected();
        }
 

        public void Send (int idEvento, int idEquipo, decimal score, int idScore)
        {
            //Esto genera una función de manera automática en java script para enviar a los clientes por push notification
            Clients.All.sendScore(idEvento, idEquipo, score, idScore);

        }
    }
}