//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Olimpiadas
{
    using System;
    using System.Collections.Generic;
    
    public partial class Puntajes
    {
        public decimal ID_EQUIPO { get; set; }
        public decimal ID_EVENTO { get; set; }
        public Nullable<decimal> PUNTAJE { get; set; }
        public decimal ID_SCORE { get; set; }
    
        public virtual Equipo Equipo { get; set; }
        public virtual EVENTO EVENTO { get; set; }
    }
}
