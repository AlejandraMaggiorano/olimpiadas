﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Olimpiadas.Models.Request
{
    public class User
    {
        public string username { get; set; }
        public string password { get; set; }
    }
}