﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Olimpiadas.Models.Request
{
    public class Medallero
    {
        public bool update { get; set; }
        public int id_medallero { get; set; }
        public decimal oro { get; set; }
        public decimal plata { get; set; }
        public decimal bronce { get; set; }
    }
}