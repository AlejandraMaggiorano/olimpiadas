﻿using Olimpiadas.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using UtilitiesCliente.Models.WS;

//OBELIX - NO CLIENTE

namespace Olimpiadas.Controllers
{
    public class ScoresController : BaseController
    {
        [HttpPost]
        public Reply List([FromBody]SecurityRequest model)
        {
            Reply rta = new Reply();
            rta.result = 0;

            if(!VerifyToken(model))
            {
                rta.message = "Método no permitido - Access Token no válido";
            }

            using (DBOlimpiadasEntities db = new DBOlimpiadasEntities())
            {
                List<ListEventoResponse> listaEventos = (from d in db.EVENTO
                                                  orderby d.TIPO_EVENTO
                                                  select new ListEventoResponse
                                                  {//Casteo el evento del modelo de base dentro de mi clase evento para evitar posibles referencias circulares
                                                      idEvento = d.ID_EVENTO,
                                                      tipoEvento = d.TIPO_EVENTO
                                                  }).ToList();
                rta.data = listaEventos;

            }
            rta.result = 1;

            return rta;
        }
    }
}
