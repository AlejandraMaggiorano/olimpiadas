﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using UtilitiesCliente.Models.WS;

namespace Olimpiadas.Controllers
{
    public class MedallerosController : ApiController
    {


        [HttpPost]
        public Reply Medalleros([FromBody]MedalleroRequest model)
        {
            Reply rta = new Reply();

            //Este método devuelve el medallero si update = false,
            //si no trata de actualizar el medallero con los datos proporcionados.
            //Devuelve 0 si no tiene éxito

            if(model.update == false)
            {
                using (DBOlimpiadasEntities db = new DBOlimpiadasEntities())
                {
                    List<ListMedalleroResponse> listaMedalleros= (from d in db.MEDALLERO
                                                             orderby d.ID_MEDALLERO
                                                             where d.ID_MEDALLERO == model.id
                                                             select new ListMedalleroResponse
                                                             {//Casteo el evento del modelo de base dentro de mi clase evento para evitar posibles referencias circulares
                                                                 oro = d.ORO,
                                                                 plata = d.PLATA,
                                                                 bronce = d.BRONCE,
                                                                 id= (int)d.ID_MEDALLERO
                                                             }).ToList();
                    rta.data = listaMedalleros;
                    rta.result = 1;


                }
            }
            else
            { 
            try
            {
                using (DBOlimpiadasEntities db = new DBOlimpiadasEntities())
                {
                    //Acá busco el medallero
                    var result = db.MEDALLERO.SingleOrDefault(b => b.ID_MEDALLERO == model.id);
                    if (result != null)
                    {
                       
                        result.ID_MEDALLERO = model.id;
                        result.ORO = model.oro;
                        result.PLATA = model.plata;
                        result.BRONCE = model.bronce;

                        db.SaveChanges();

                            List<ListMedalleroResponse> listaMedalleros = (from d in db.MEDALLERO
                                                                           orderby d.ID_MEDALLERO
                                                                           where d.ID_MEDALLERO == model.id
                                                                           select new ListMedalleroResponse
                                                                           {//Casteo el evento del modelo de base dentro de mi clase evento para evitar posibles referencias circulares
                                                                               oro = d.ORO,
                                                                               plata = d.PLATA,
                                                                               bronce = d.BRONCE,
                                                                               id = (int)d.ID_MEDALLERO
                                                                           }).ToList();
                            rta.data = listaMedalleros;

                            rta.result = 1;
                    }else
                    { rta.result = 0;  //Fallé al encontrar el medallero
                    }
                }
            }//try
            catch (Exception e)
            {
            }//catch
            }//else
            return rta;
        }



    }
}
