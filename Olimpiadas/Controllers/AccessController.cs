﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using UtilitiesCliente.Models.WS;
using Olimpiadas.Models;


namespace Olimpiadas.Controllers
{
    public class AccessController : ApiController
    {
       

           [HttpPost]
        public Reply Login(AccessRequest model)
        {
    
            Reply rta = new Reply();
            rta.result = 0;

            using (DBOlimpiadasEntities db = new DBOlimpiadasEntities() )
            {
                var Usuario = (from d in db.user
                              where d.username == model.Username && d.password == model.Password
                              select d).FirstOrDefault();
                if(Usuario != null)
                {
                    string AccessToken = Guid.NewGuid().ToString();

                    Usuario.acces_token = AccessToken;
                    db.Entry(Usuario).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();

                    UserResponse usrresponse = new UserResponse();
                    usrresponse.AccessToken = AccessToken;
                    usrresponse.UserName = Usuario.username;

                    rta.result = 1;
                    rta.data = usrresponse;


                }
                else
                {
                    rta.message = "Datos inválidos de usuario o contraseña.";
                }

            }


                return rta;
        }
    }
}
