﻿
using Olimpiadas.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using UtilitiesCliente.Models.WS;

namespace Olimpiadas.Controllers
{
    public class UserController : ApiController
    {
      /*  [HttpGet]
        public Reply Get()
        {
            Reply rta = new Reply();

            using (Models.OlimpiadasDBEntities db = new Models.OlimpiadasDBEntities())
            {
                List<UserViewModel> lista = (from d in db.user
                                             select new UserViewModel
                                             { name = d.username, fecha = d.date_created }
                                              ).ToList<UserViewModel>();
                rta.result = 1;
                rta.data = lista;
            }
         
            return rta;
        }
            */

        [HttpPost]
        public Reply Register([FromBody] Models.Request.User model)
        {
            Reply rta = new Reply();

            try { 
            using (DBOlimpiadasEntities db = new DBOlimpiadasEntities())
            {
                user Usuario = new user();
                Usuario.username = model.username;
                Usuario.password = model.password;
                Usuario.date_created = DateTime.Now;

                db.user.Add(Usuario);
                db.SaveChanges();
                rta.result = 1;
            }
            } catch (Exception e)
            {
            }
            return rta;
        }
            
     }

        
    
}
