﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using UtilitiesCliente.Models.WS;

namespace Olimpiadas.Controllers
{
    public class ScoreUpdateController : BaseController
    {
        [HttpPost]
        public Reply ScoreUpdate([FromBody]ScoreUpdateRequest model)
        {
            Reply rta = new Reply();

            //Este método devuelve la lista de equipos y sus puntajes si update = false,
            //si no trata de actualizar con los datos proporcionados.
            //Devuelve 0 si no tiene éxito

            if (model.update == true)
            {
                try
                {
                    using (DBOlimpiadasEntities db = new DBOlimpiadasEntities())
                    {
                        //Acá busco el puntaje para actualizar

                        var result = db.Puntajes.SingleOrDefault(b => b.ID_EQUIPO == model.idEquipo && b.ID_EVENTO == model.idEvento);
                        if (result != null)
                        {

                            result.PUNTAJE = model.puntaje;


                            db.SaveChanges();

                        }
                        else
                        {
                            rta.result = 0;  //Fallé al encontrar el medallero
                        }
                    }
                }//try
                catch (Exception e)
                {
                }//catch


                using (DBOlimpiadasEntities db = new DBOlimpiadasEntities())
                {
                    //Esto hace inner join según la página oficial de entity framework y otro tutorial que no me acuerdo de dónde lo saqué
                    List<ListScoreResponse> listaPuntajess = (from d in db.Equipo
                                                              join s in db.Puntajes on d.ID_Equipo equals s.ID_EQUIPO
                                                              where s.PUNTAJE == model.puntaje
                                                              select new ListScoreResponse
                                                              {//Casteo el evento del modelo de base dentro de mi clase evento para evitar posibles referencias circulares
                                                                  id_equipo = (int)d.ID_Equipo,
                                                                  id_evento = (int)s.ID_EVENTO,
                                                                  score = (int)s.PUNTAJE,
                                                                  descripcion_equipo = d.NOMBRE_EQUIPO,
                                                                  id_score = (int)s.ID_SCORE
                                                              }).ToList();

                    rta.data = listaPuntajess;
                    rta.result = 1;


                }

            }//Update == true
            else
            {


                if (model.idEvento == 0 && model.idEquipo == 0 && model.idScore != 0)
                {
                    using (DBOlimpiadasEntities db = new DBOlimpiadasEntities())
                    {
                        //Esto hace inner join según la página oficial de entity framework y otro tutorial que no me acuerdo de dónde lo saqué
                        List<ListScoreResponse> listaPuntajess = (from d in db.Equipo
                                                                  join s in db.Puntajes on d.ID_Equipo equals s.ID_EQUIPO
                                                                  where s.ID_SCORE == model.idScore
                                                                  select new ListScoreResponse
                                                                  {//Casteo el evento del modelo de base dentro de mi clase evento para evitar posibles referencias circulares
                                                                      id_equipo = (int)d.ID_Equipo,
                                                                      id_evento = (int)s.ID_EVENTO,
                                                                      score = (int)s.PUNTAJE,
                                                                      descripcion_equipo = d.NOMBRE_EQUIPO,
                                                                      id_score = (int)s.ID_SCORE
                                                                  }).ToList();

                        rta.data = listaPuntajess;
                        rta.result = 1;


                    }
                }
                else
                {
                    using (DBOlimpiadasEntities db = new DBOlimpiadasEntities())
                    {
                        //Esto hace inner join según la página oficial de entity framework y otro tutorial que no me acuerdo de dónde lo saqué
                        List<ListScoreResponse> listaPuntajess = (from d in db.Equipo
                                                                  join s in db.Puntajes on d.ID_Equipo equals s.ID_EQUIPO
                                                                  where s.ID_EVENTO == model.idEvento
                                                                  select new ListScoreResponse
                                                                  {//Casteo el evento del modelo de base dentro de mi clase evento para evitar posibles referencias circulares
                                                                      id_equipo = (int)d.ID_Equipo,
                                                                      id_evento = (int)s.ID_EVENTO,
                                                                      score = (int)s.PUNTAJE,
                                                                      descripcion_equipo = d.NOMBRE_EQUIPO,
                                                                      id_score = (int)s.ID_SCORE
                                                                  }).ToList();

                        rta.data = listaPuntajess;
                        rta.result = 1;


                    }
                }//else id evento equipo nulas
            }//else
  
            
            return rta;
        }



    }
}
