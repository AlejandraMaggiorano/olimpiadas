﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Olimpiadas.Models;
using UtilitiesCliente.Models.WS;

//OBELIX - NO CLIENTE

namespace Olimpiadas.Controllers
{
    public class EquiposController : BaseController
    {
        // NO ES SEGURO - SIN TOKEN
        [HttpPost]
        public Reply List()
        {
            Reply rta = new Reply();
            rta.result = 0;


            using (DBOlimpiadasEntities db = new DBOlimpiadasEntities())
            {
                List<ListEquiposResponse> listaEquipos = (from d in db.Equipo
                                                         orderby d.NOMBRE_EQUIPO
                                                         select new ListEquiposResponse
                                                         {//Casteo el evento del modelo de base dentro de mi clase evento para evitar posibles referencias circulares
                                                             idEquipo = d.ID_Equipo,
                                                             nombreEquipo = d.NOMBRE_EQUIPO,
                                                             idMedallero = d.ID_MEDALLERO
                                                         }).ToList();
                rta.data = listaEquipos;

            }
            rta.result = 1;

            return rta;
        }
    }
}
