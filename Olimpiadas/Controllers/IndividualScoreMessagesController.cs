﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using UtilitiesCliente.Models.WS;
using Olimpiadas.Models;

namespace Olimpiadas.Controllers
{
    public class IndividualScoreMessagesController : ApiController
    {
        //Pendiente probar en fiddler

        //Este controlador era de los mensajes de chat en el ejemplo, debería controlar el pasaje de puntajes en este proyecto
        [HttpPost]
        public Reply Get([FromBody] ScoreRequest model)
        {
            Reply rta = new Reply();
            rta.result = 0;
            try

               
        
            {
                using (DBOlimpiadasEntities db = new DBOlimpiadasEntities())
                {
                    List<MessageResponse> lst = (from e in db.Equipo
                                                 join d in db.Puntajes on e.ID_Equipo equals d.ID_EQUIPO
                                                 where d.ID_EVENTO == model.idEvento
                                                 select new MessageResponse
                                                  {
                                                      Puntaje = d.PUNTAJE,
                                                      idEquipo = d.ID_EQUIPO,
                                                      idEvento = d.ID_EVENTO,
                                                      nombreEquipo = e.NOMBRE_EQUIPO

                                                  }).ToList();
                    rta.result = 1;
                    rta.data = lst;
                }
            }
            catch(Exception e)
            {
                rta.message = "Error durante el pasaje de puntajes";
            }

            return rta;
        }
    }
}
