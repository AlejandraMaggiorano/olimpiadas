﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using UtilitiesCliente.Models.WS;
using Olimpiadas.Models;

namespace Olimpiadas.Controllers
{

    public class BaseController : ApiController
    {
        UserResponse rtaUsuarioSesion;

     //Esto sirve para manejo de seguridad - video 12
     
        protected bool VerifyToken( SecurityRequest model  )
        {
            using (DBOlimpiadasEntities db = new DBOlimpiadasEntities())
            {
                var usuario = db.user.Where(d => d.acces_token.ToUpper() == model.AccessToken.ToUpper()).FirstOrDefault();

                if(usuario != null )
                {
                    rtaUsuarioSesion = new UserResponse();
                    rtaUsuarioSesion.AccessToken = usuario.acces_token;
                    rtaUsuarioSesion.UserName = usuario.username;

                    return true;
                }
            }
            return false;

        }
    }
}
