﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using UtilitiesCliente.Models.WS;

namespace Olimpiadas.Controllers
{
    public class EventosController : ApiController
    {

        [HttpPost]
        public Reply Medalleros([FromBody]EventosRequest model)
        {
            Reply rta = new Reply();

            //Este método devuelve  si update = false,
            //si no trata de actualizar el medallero con los datos proporcionados.
            //Devuelve 0 si no tiene éxito

                try
                {
                    using (DBOlimpiadasEntities db = new DBOlimpiadasEntities())
                    {
                   
                        
                            List<ListEventoResponse> listaEventos = (from d in db.EVENTO
                                                                           orderby d.ID_EVENTO
                                                                          
                                                                           select new ListEventoResponse
                                                                           {//Casteo el evento del modelo de base dentro de mi clase evento para evitar posibles referencias circulares

                                                                               idEvento = d.ID_EVENTO,
                                                                               tipoEvento = d.TIPO_EVENTO
                                                                           }).ToList();
                            rta.data = listaEventos;

                            rta.result = 1;
                    
                    }
                }//try
                catch (Exception e)
                {
                }//catch
        
            return rta;
        }



    }
}
