﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNet.SignalR;
using Microsoft.Owin;
using Microsoft.Owin.Cors;
using Owin;

[assembly: OwinStartup(typeof(Olimpiadas.Startup))]

//Esto se genera con add- new item - owin
namespace Olimpiadas
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            app.Map("/signalr", map =>
            {
                //Este coso permite admitir las solicitudes de cualquier IP. Owin se encarga del permiso, SignalR atiende las conexiones
                map.UseCors(CorsOptions.AllowAll); //AllowAll permite todas las IPS

                var hubConfiguration = new HubConfiguration { };
                map.RunSignalR(hubConfiguration);
            }
            );
        }
    }
}
