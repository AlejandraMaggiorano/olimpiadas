﻿using ClienteWeb.Business;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UtilitiesCliente.Models.WS;
using UtilitiesCliente.Tools;

namespace ClienteWeb.Controllers
{
    public class CacofonixScoreController : Controller
    {
        // GET: CacofonixScore
        [HttpGet]
        public ActionResult Index()
        {

            //   *** 1 ***
            List<ListEventoResponse> lst = new List<ListEventoResponse>();


            RequestUtil ReqUtil = new RequestUtil();
            Reply rta = ReqUtil.Execute<ListEquiposResponse>(Constants.Url.EVENTOS, "post");

            lst = JsonConvert.DeserializeObject<List<ListEventoResponse>>(JsonConvert.SerializeObject(rta.data));


            return View(lst);
        }//Index

        [HttpPost]
        public ActionResult Index(int id)
        {

            //   *** 1 ***
            ScoreUpdateRequest model = new ScoreUpdateRequest();
            model.update = false;
            model.idEvento = id;


            List<ScoreUpdateRequest> lst = new List<ScoreUpdateRequest>();


            RequestUtil ReqUtil = new RequestUtil();
            Reply rta = ReqUtil.Execute<ScoreUpdateRequest>(Constants.Url.SCOREUPDATE, "post", model);

            lst = JsonConvert.DeserializeObject<List<ScoreUpdateRequest>>(JsonConvert.SerializeObject(rta.data));

            lst = lst.OrderBy(d => d.idEquipo).ToList();

            if (rta.result != 0)
            {
                ScoreUpdateRequest recibido = lst.First<ScoreUpdateRequest>();
                return RedirectToAction("setScore", "CacofonixScore",model); //redirecciona al método de abajo que genera la vista del sitio que viene
            }

            return View(model);
        }//Index


        [HttpGet]
        public ActionResult setScore(int id)
        {

            //   *** 2 ***
           ScoreUpdateRequest model = new ScoreUpdateRequest();
            model.update = false;
            model.idEvento = id;



            List<ListScoreResponse> lst = new List<ListScoreResponse>();


            RequestUtil ReqUtil = new RequestUtil();
            Reply rta = ReqUtil.Execute<ScoreUpdateRequest>(Constants.Url.SCOREUPDATE, "post", model);

            lst = JsonConvert.DeserializeObject<List<ListScoreResponse>>(JsonConvert.SerializeObject(rta.data));

            lst = lst.OrderBy(d => d.id_equipo).ToList();

            ListScoreResponse recibido = new ListScoreResponse();
            if (rta.result != 0)
            {
                 recibido = lst.First<ListScoreResponse>();
            }

            return View(recibido);
        }//setScore

       

        [HttpGet]
        public ActionResult seleccionEquipo(int id)
        {

            //   *** 3 ***

            ScoreUpdateRequest model = new ScoreUpdateRequest();
            model.update = false;
            model.idEvento = id;



            List<ListScoreResponse> lst = new List<ListScoreResponse>();


            RequestUtil ReqUtil = new RequestUtil();
            Reply rta = ReqUtil.Execute<ScoreUpdateRequest>(Constants.Url.SCOREUPDATE, "post", model);

            lst = JsonConvert.DeserializeObject<List<ListScoreResponse>>(JsonConvert.SerializeObject(rta.data));

            lst = lst.OrderBy(d => d.id_equipo).ToList();

            if (rta.result != 0)
            {
                ListScoreResponse recibido = lst.First<ListScoreResponse>();
            }

            return View(lst);
        }//setScore


        [HttpPost]
        public ActionResult setScore(ListScoreResponse model)
        {

            //   *** 3 ***

            model.update = true;

            List<ListScoreResponse> lst = new List<ListScoreResponse>();
            ScoreUpdateRequest mdl = new ScoreUpdateRequest();
            mdl.idEquipo = model.id_equipo;
            mdl.update = true;
            mdl.puntaje = model.score;
            mdl.idScore = model.id_score;
            mdl.idEvento = model.id_evento;
            mdl.nombreEquipo = model.descripcion_equipo;
            

            RequestUtil ReqUtil = new RequestUtil();
            Reply rta = ReqUtil.Execute<ScoreUpdateRequest>(Constants.Url.SCOREUPDATE, "post", mdl);

            lst = JsonConvert.DeserializeObject<List<ListScoreResponse>>(JsonConvert.SerializeObject(rta.data));

            lst = lst.OrderBy(d => d.id_score).ToList();

            ListScoreResponse recibido = new ListScoreResponse();
            if (rta.result != 0)
            {
                 recibido = lst.First<ListScoreResponse>();
            }

            ViewBag.error = "Puntaje guardado";

            return View(recibido);
        }//setScore

    }//Cacofonixscorecontroller
}