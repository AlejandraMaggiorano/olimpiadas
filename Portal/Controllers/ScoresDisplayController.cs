﻿using ClienteWeb.Business;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UtilitiesCliente.Models.WS;
using UtilitiesCliente.Tools;


//Genera la vista de puntajes desde el lado del cliente web
namespace ClienteWeb.Controllers
{
    public class ScoresDisplayController : BaseController
    {       
        public ActionResult ScoresDisplay(int id)
        {

            GetSession();

            List<MessageResponse> lst = new List<MessageResponse>();

            ScoreRequest msj = new ScoreRequest();
            msj.AccessToken = sesion.AccessToken;
            msj.idEvento = id;

            RequestUtil req = new RequestUtil();
            Reply rta = req.Execute<ScoreRequest>(Constants.Url.INDIVIDUALSCORES, "post", msj);

            lst = JsonConvert.DeserializeObject<List<MessageResponse>>(JsonConvert.SerializeObject(rta.data));

            lst = lst.OrderBy(d => d.Puntaje).ToList();

            return View(lst);
        }
    }
}