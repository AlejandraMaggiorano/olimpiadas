﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UtilitiesCliente.Models.WS;

namespace ClienteWeb.Controllers
{
    public class BaseController : Controller
    {
        // Controlador de base que tiene todo lo referente a accesibilidad

        public UserResponse sesion;



        protected void GetSession()
        {
            sesion = (UserResponse)Session["User"];
        }

    }
}