﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace ClienteWeb.Business
{
    public class Constants
    {
        //Acá se lee el web.config

        public static string URL_API
        {
            get
            {
                return ConfigurationManager.AppSettings["url_ws"];
            }
        }


        public class Url
        {
            public static string REGISTER
            {
                get
                {
                    return URL_API + "api/User/";
                }

            }

            public static string SignalR
            {
                get
                {
                    return URL_API + "signalr/";
                }

            }

            public static string SignalRHub
            {
                get
                {
                    return URL_API + "signalr/hubs";
                }

            }

            public static string SCORES
            {
                get
                {
                    return URL_API + "api/Scores";
                }

            }
            public static string SCOREUPDATE
            {
                get
                {
                    return URL_API + "api/ScoreUpdate";
                }

            }

            public static string EVENTOS
            {
                get
                {
                    return URL_API + "api/Eventos";
                }

            }
            public static string EQUIPOS
            {
                get
                {
                    return URL_API + "api/Equipos";
                }

            }

            public static string MEDALLEROS
            {
                get
                {
                    return URL_API + "api/Medalleros";
                }

            }

            public static string INDIVIDUALSCORES
            {
                get
                {
                    return URL_API + "api/IndividualScoreMessages";
                }

            }

            public static string ACCESS
            {
                get
                {
                    return URL_API + "api/Access";
                }

            }

        }

    }
    
}