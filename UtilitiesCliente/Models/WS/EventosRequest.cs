﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UtilitiesCliente.Models.WS
{
    public class EventosRequest
    {
        public int idEvento { get; set; }
        public string tipoEvento { get; set; }
    }
}
