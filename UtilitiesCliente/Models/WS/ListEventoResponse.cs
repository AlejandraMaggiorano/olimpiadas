﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UtilitiesCliente.Models.WS
{
     public class ListEventoResponse
    {
       
        public string tipoEvento { get; set; }
        public Decimal idEvento { get; set; }

    }
}
