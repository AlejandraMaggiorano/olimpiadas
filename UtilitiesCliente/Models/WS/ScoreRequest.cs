﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UtilitiesCliente.Models.WS
{
    public class ScoreRequest
    {
        public int idEvento { get; set; }
        public string AccessToken { get; set; }

    }
}
