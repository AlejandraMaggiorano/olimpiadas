﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UtilitiesCliente.Models.WS
{
    public class ListMedalleroResponse
    {
        public int id { get; set; }
        public decimal oro { get; set; }
        public decimal plata { get; set; }
        public decimal bronce { get; set; }
    }
}
