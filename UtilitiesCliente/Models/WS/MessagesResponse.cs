﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UtilitiesCliente.Models.WS
{
    public class MessageResponse
    {
        public decimal? Puntaje {  get; set; }
        public decimal? idEvento { get; set; }
        public decimal? idEquipo { get; set; }
        public string nombreEquipo { get; set; }
    }
}
