﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UtilitiesCliente.Models.WS
{
   public class ListScoreResponse
    {
        public int id_score { get; set; }
        public bool update { get; set; }
        public string descripcion_equipo { get; set; }
        public int id_equipo { get; set; }
        public int id_evento { get; set; }
        public decimal score { get; set; }
    }
}
