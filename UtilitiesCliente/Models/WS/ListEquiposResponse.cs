﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UtilitiesCliente.Models.WS
{
     public class ListEquiposResponse
    {
            public Decimal idEquipo { get; set; }
            public string nombreEquipo { get; set; }
            public Decimal idMedallero { get; set; }

    }
}
