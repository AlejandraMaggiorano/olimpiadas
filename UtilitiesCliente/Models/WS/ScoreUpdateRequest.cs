﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UtilitiesCliente.Models.WS
{
   public class ScoreUpdateRequest
    {

        public int idEvento { get; set; }
        public int idEquipo { get; set; }
        public string nombreEquipo { get; set; }
        public decimal puntaje { get; set; }
        public bool update { get; set; }
        public int idScore { get; set; }


    }

}

