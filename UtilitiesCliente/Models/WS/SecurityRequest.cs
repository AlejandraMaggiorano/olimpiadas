﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UtilitiesCliente.Models.WS
{
    //este objeto va a ser la madre y heredar  hijos para agregar seguridad

    public class SecurityRequest
    {
        public string AccessToken { get; set; }
    }
}
