﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using UtilitiesCliente.Models.WS;

namespace UtilitiesCliente.Tools
{
    public class RequestUtil
    {
        public Reply rta { get; set; }
        public RequestUtil()
        {
            rta = new Reply();
        }

        public Reply Execute<T>(string url, string method, T objectRequest)
        {
            try
            {
                //Esto lee y transforma en json a un objetos, clase genérica a ser reutilizada 

                JavaScriptSerializer js = new JavaScriptSerializer();
                string json = JsonConvert.SerializeObject(objectRequest);
                string result = "";
                WebRequest request = WebRequest.Create(url);
                request.Method = method; //Esto es POST, etc el método de acceso
                request.ContentType = "application/json;charset=utf-8"; //Esto sería json con un set de caracteres compatible con ñ
                request.Timeout = 60000; //1 minuto en milésimas de segundo

                using (var streamwrite = new StreamWriter(request.GetRequestStream())) //Genera un flujo para el RequestBody
                {
                    streamwrite.Write(json);
                    streamwrite.Flush();
                }

                //Acá obtengo la respuesta
                var httpRespuesta = (HttpWebResponse)request.GetResponse();
                using (StreamReader StreamRead = new StreamReader(httpRespuesta.GetResponseStream()))
                {
                    result = StreamRead.ReadToEnd(); //Esto contiene un JSON de respuesa
                }

                rta = JsonConvert.DeserializeObject<Reply>(result); //Esto vuelve el JSON a objeto Reply
            }
            catch (TimeoutException e)
            {
                rta.result = 0;
                rta.message = "Tiempo de espera agotado";
            }
            catch (Exception e)
            {
                rta.result = 0;
                rta.message = "Excepción al ejecutar el cliente: " + e.Message;
            }

            return rta;

        }




        public Reply Execute<T>(string url, string method)
        {
            try
            {
                //Esto lee y transforma en json a un objetos, clase genérica a ser reutilizada - sin seguridd

                JavaScriptSerializer js = new JavaScriptSerializer();
               // string json = JsonConvert.SerializeObject(objectRequest);
                string result = "";
                WebRequest request = WebRequest.Create(url);
                request.Method = method; //Esto es POST, etc el método de acceso
                request.ContentType = "application/json;charset=utf-8"; //Esto sería json con un set de caracteres compatible con ñ
                request.Timeout = 60000; //1 minuto en milésimas de segundo

                using (var streamwrite = new StreamWriter(request.GetRequestStream())) //Genera un flujo para el RequestBody
                {
                 //   streamwrite.Write(json);
                    streamwrite.Flush();
                }

                //Acá obtengo la respuesta
                var httpRespuesta = (HttpWebResponse)request.GetResponse();
                using (StreamReader StreamRead = new StreamReader(httpRespuesta.GetResponseStream()))
                {
                    result = StreamRead.ReadToEnd(); //Esto contiene un JSON de respuesa
                }

                rta = JsonConvert.DeserializeObject<Reply>(result); //Esto vuelve el JSON a objeto Reply
            }
            catch (TimeoutException e)
            {
                rta.result = 0;
                rta.message = "Tiempo de espera agotado";
            }
            catch (Exception e)
            {
                rta.result = 0;
                rta.message = "Excepción al ejecutar el cliente: " + e.Message;
            }

            return rta;

        }


    }
}
