﻿using PortalCacofonix.Business;
using PortalCacofonix.Models.ViewModels;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UtilitiesCliente.Models.WS;
using UtilitiesCliente.Tools;

//Cada método de los que hay en un controlador y que devuelvan un Action Result
//puede generar una vista. La URL es ip:puerto/controlador/metodo


namespace PortalCacofonix.Controllers
{
    public class CacofonixMedalleroController : Controller
    {
        // GET: CacofonixMedallero
        [HttpGet]
            public ActionResult Index()
            {
                List<ListEquiposResponse> lst = new List<ListEquiposResponse>();


                RequestUtil ReqUtil = new RequestUtil();
                Reply rta = ReqUtil.Execute<ListEquiposResponse>(Constants.Url.EQUIPOS, "post");

                lst = JsonConvert.DeserializeObject<List<ListEquiposResponse>>(JsonConvert.SerializeObject(rta.data));


                return View(lst);
            }//Index

        [HttpPost]
        public ActionResult Index( MedalleroViewModel modelo)
        {
            MedalleroRequest model = new MedalleroRequest();
            model.update = false;
            model.id = modelo.id;


            List<ListMedalleroResponse> lst = new List<ListMedalleroResponse>();


            RequestUtil ReqUtil = new RequestUtil();
            Reply rta = ReqUtil.Execute<MedalleroRequest>(Constants.Url.MEDALLEROS, "post", model);

            lst = JsonConvert.DeserializeObject<List<ListMedalleroResponse>>(JsonConvert.SerializeObject(rta.data));

            lst = lst.OrderBy(d => d.id).ToList();

            if (rta.result != 0)
            {
                ListMedalleroResponse recibido = lst.First<ListMedalleroResponse>();
                return RedirectToAction("IncrementMedallero", "CacofonixMedallero"); //redirecciona al método de abajo que genera la vista del sitio que viene
            }

            return View(model);
        }//Index

        [HttpGet]
    public ActionResult IncrementMedallero(int id)
    {
            MedalleroRequest model = new MedalleroRequest();
            model.update = false;
            model.id = id;

        List<ListMedalleroResponse> lst = new List<ListMedalleroResponse>();


        RequestUtil ReqUtil = new RequestUtil();
        Reply rta = ReqUtil.Execute<MedalleroRequest>(Constants.Url.MEDALLEROS, "post", model);

            lst = JsonConvert.DeserializeObject<List<ListMedalleroResponse>>(JsonConvert.SerializeObject(rta.data));

            lst = lst.OrderBy(d => d.id).ToList();

            if (rta.result != 0)
            {
                ListMedalleroResponse recibido = lst.First<ListMedalleroResponse>();
            }

            return View(model);
    }//IncrementMedallero


          [HttpPost]
       public ActionResult IncrementMedallero(MedalleroRequest model)
       {

           
            model.update = true;

            List<ListMedalleroResponse> lst = new List<ListMedalleroResponse>();


            RequestUtil ReqUtil = new RequestUtil();
            Reply rta = ReqUtil.Execute<MedalleroRequest>(Constants.Url.MEDALLEROS, "post", model);

            lst = JsonConvert.DeserializeObject<List<ListMedalleroResponse>>(JsonConvert.SerializeObject(rta.data));

            lst = lst.OrderBy(d => d.id).ToList();

            if (rta.result != 0)
            {
                ListMedalleroResponse recibido = lst.First<ListMedalleroResponse>();
            }

            ViewBag.error = "Medallero guardado";

            return View(model);
        }//IncrementMedallero
    }

}

