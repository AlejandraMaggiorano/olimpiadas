﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PortalCacofonix.Models.ViewModels
{
    public class MedalleroViewModel
    {
        public int id { get; set; }
        public int oro { get; set; }
        public int plata { get; set; }
        public int bronce { get; set; }
    }
}